import Data.List

-- Problem 1
problem_1 = sum [ x |  x <- [1..999], x `mod` 3 == 0 || x `mod` 5 == 0]

-- Problem 3
largestPrimeFactor n =
	let	p = smallestPrimeFactor n
	in
		if p == n 
			then p
			else largestPrimeFactor (n `div` p)
smallestPrimeFactor n = head [k | k <- [2..n], n `mod` k == 0]
problem_3 = largestPrimeFactor 600851475143

-- Problem 4
problem_4 = maximum [x*y | x <- [100..999], y <- [x..999], let s = show (x*y), s == reverse s]

-- Problem 5
-- http://www.ibm.com/developerworks/ru/library/l-haskell4/
-- lcm - lowest common multiple
problem_5 = foldr1 lcm [1..20]

-- Problem 9
p = 1000;
isRightTriangle a b c = a < b && b < c && a * a + b * b == c * c
problem_9 = head [a * b * (p - a - b) | a <- [1..p], b <- [a+1..p], isRightTriangle a b (p - a - b)]

-- Problem 16
digitSumma 0 = 0
digitSumma n = (n `mod` 10) + (digitSumma (n `div` 10))
problem_16 = digitSumma (2^1000)

-- Problem 21
isAmicable n = let m = divisorSum n in (m /= n) && (divisorSum m) == n
divisorSum n = sum [k | k <- [1..n-1], (mod n k) == 0]
problem_21 = sum [n | n <- [1..10^4], isAmicable n]

-- Problem 28
size = 1001
problem_28 = 1 + sum [4 * n * n - 6 * (n - 1) | n <- [3, 5 .. size]]

-- Problem 29
-- https://www.haskell.org/hoogle/?hoogle=nub
problem_29 = length $ nub [x^y | x <- [2..100], y <- [2..100]]


-- Problem 30
fifthPowerDigitSum 0 = 0
fifthPowerDigitSum n = (n `mod` 10)^5 + (fifthPowerDigitSum (n `div` 10))
problem_30 = sum [i | i <- [2..10^6 - 1], i == fifthPowerDigitSum i]

-- Problem 34
factorial = [1, 1, 2, 6, 24, 120, 720, 5040, 40320, 362880]
factorialDigitSum 0 = 0
factorialDigitSum n = (factorial !! (n `mod` 10)) + (factorialDigitSum (n `div` 10))
problem_34 = sum [i | i <- [3..10^6 - 1], i == factorialDigitSum i]


-- Problem 53
binomial n r = (product [n-r+1..n]) `div` (product [1..r])
problem_53 = length [1 | n <- [1..100], r <- [0..n], binomial n r > 10^6]

-- Problem 56
digitsum 0 = 0
digitsum n = (n `mod` 10) + (digitsum (n `div` 10))
problem_56 = maximum [digitsum (a^b) | a <- [0..99], b <- [0..99]]

-- Problem 92
arrive89 :: Int -> Bool
arrive89 1 = False
arrive89 89 = True
arrive89 n = arrive89 (squareDigitSum n)
squareDigitSum 0 = 0
squareDigitSum n = (n `mod` 10)^2 + (squareDigitSum (n `div` 10))
problem_92 = length (filter arrive89 [1 .. (10^7 - 1)])

-- Problem 100
-- диофантово квадратичное уравнение
nextBlue blue summa 
    |summa > 10^12 = blue
    |otherwise = nextBlue (3*blue+2*summa-2) (4*blue+3*summa-3)
problem_100 = nextBlue 15 21

-- Problem 117
ways 0 = 1
ways n = sum $ take (min n 4) $ drop (max (n-4) 0) listOfWays
listOfWays = map ways [0..]
problem_117 = ways 50


